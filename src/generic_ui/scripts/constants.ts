// Icons for browser bar, also used for notifications.
export var DEFAULT_ICON = 'online.gif';
export var LOGGED_OUT_ICON = 'offline.gif';
export var SHARING_ICON = 'sharing.gif';
export var GETTING_ICON = 'getting.gif';
export var ERROR_ICON = 'error.gif';
export var GETTING_SHARING_ICON = 'gettingandsharing.gif';

export var DEFAULT_USER_IMG = '../icons/contact-default.png';
